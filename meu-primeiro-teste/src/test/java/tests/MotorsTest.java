package tests;

import org.junit.*;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.List;
import java.io.File;
import java.io.OutputStream;
import java.util.Locale;

public class MotorsTest {
    @Test
    @Ignore
    public void fazerBuscaWeb() throws InterruptedException {
        System.setProperty("webserver.chrome.driver", "C:\\GoogleDriver\\chromedriver");
        WebDriver navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.get("https://www.webmotors.com.br/");
        navegador.findElement(By.id("searchBar")).sendKeys("Honda City");
        Thread.sleep(Long.parseLong("1000"));
        List<WebElement> LIST = navegador
        .findElements(By.xpath("//*[@id=\"WhiteBox\"]/div[1]/div[2]/div/div/div/div/div/div/a"));
        System.out.println(LIST.size());

        for (int i = 0; i < LIST.size(); i++) {
            System.out.println(LIST.get(i).getText());
            if (LIST.get(i).getText().contains("Honda City")){
                LIST.get(i).click();


           }
            Thread.sleep(Long.parseLong("1000"));

            WebElement resultadoPesquisa = navegador.findElement(By.className("ContainerCardVehicle"));
            String resultado = resultadoPesquisa.getText();
            resultado = resultado.toUpperCase();

           Assert.assertTrue(resultado.contains("AUTOMÁTICO"));
           Assert.assertTrue(resultado.contains("HONDA CITY"));
           Assert.assertFalse(resultado.contains("HONDA CIVIV"));
        }
    }
}